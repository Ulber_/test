<?php
  $valor1 = 13;
  $valor2 = "13";

  //==, ===, != o <>, !==
  var_dump($valor1 == $valor2);

  $a= 13;
  $b= 14;
  //>, <, >=, <=
  var_dump($a > $b);

//dato = 1, 0 "hola", NULL
  $dato = 1;
  echo "<br />";
  var_dump($dato?:"no hay dato");
  echo "<br />";
  var_dump($dato?"si hay dato":"no hay dato");
  echo "<br />";
  var_dump($dato??"es null");
?>
