<?php
class Persona{
  //Atributos  
  public $nombre = "Pedro";
  //Metodos
  public function hablar($mensaje) {
    echo $mensaje;
  }
}

  $Persona = new Persona();
  $Persona->hablar("Saludos desde softwin Perú");


class Calculadora{
  public $ope1;
  public $ope2;
  public $res;

  public function suma($num1, $num2){
    $this->ope1 = $num1 + $num2;

  }
  public function multiplicacion($var1, $var2){
    $this->ope2 = $var1 * $var2;

  }
  public function resultado(){
    $this->res = $this->ope1 + $this->ope2;
    echo $this->res;

  }
}
/*
$Calculadora = new Calculadora;
$Calculadora->suma(3, 5);
$Calculadora->multiplicacion(2, 4);
$Calculadora->resultado();
*/
?>
