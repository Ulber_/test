<?php

//forma 1
$a[]=2;
$a[]=5;
$a[5.925]=34;

$b[]="hola";
$b[]="adios";
$b[8]="Otro";
$b["valor1"]="asociando";

/*
echo "<pre>";
print_r($a);

echo "<pre>";
print_r($b);
*/
//forma 2

$array=array("hola", "yes"=>"true", "dos", 3, "prueba");

//print_r($array);
/*
foreach($array as $clave => $valor)
{ echo "Mirar -->".$valor."<pre>";
};
*/
//Arrays Multidimensionales o anidados

  $personas = array(
    array("Jorge","Ramirez",25),
    array("Saul","Velez",32),
    array("Rodrigo","Perez",22),
    array("Roger","Cruz",20),
    array("Raul","Solar",16),
  );
  //Obtener un valor
  echo "hola".$personas[0][1]."\n";
  $fila = 1;
  $columna = 1;  
  echo "Adios".$personas[$fila][$columna];
/*
  //Recorrer un array indexado
  for($fila=0; $fila<count($personas); $fila++):
  for($columna=0; $columna<count($personas[$fila]); $columna++):

    echo "<pre>".$personas[$fila][$columna];
  endfor;
  endfor;
*/
  //Recorrer un array mixto
    $datos= array(
    "A" => array("Italia","Peru",2),
    "B" => array("Brazil",3,9),
    "C" => array(8,"Alemania",1),
    "D" => array(8,9,2),
    "E" => array("Israel","Croacia","Dinamarca"),
  );
/*
  foreach($datos as $clave => $valor)
    {for($indice=0; $indice<count($valor); $indice++):
      echo $valor[$indice]. "\n";
    endfor;
  };
*/
?>
